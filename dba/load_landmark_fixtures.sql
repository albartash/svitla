INSERT INTO public.landmarks(label, coordinates) VALUES (
    'Statue of Old Man with Large Hat',
    POINT(500, 166)
);

INSERT INTO public.landmarks(label, coordinates) VALUES (
    'The Opera House',
    POINT(123, 501)
);
