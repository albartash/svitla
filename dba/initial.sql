CREATE TABLE public.landmarks(
  id SERIAL,
  label VARCHAR(200),
  coordinates POINT,
  created TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

CREATE TABLE public.routes(
  id serial,
  name VARCHAR(100),
  instructions text array,
  created TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);
