INSERT INTO public.routes(name, instructions) VALUES(
    'First',
    ARRAY[
          'Start at (245, 161).',
          'Go North 5 blocks.',
          'Turn right.',
          'Go until you reach landmark "Statue of Old Man with Large Hat".',
          'Go West 25 blocks.',
          'Turn left.',
          'Go 3 blocks.'
    ]);


INSERT INTO public.routes(name, instructions) VALUES(
    'Another one',
    ARRAY[
          'Start at (123, 100).',
          'Go South 5 blocks.',
          'Go until you reach landmark "The Opera House".',
          'Turn right.',
          '# Empty lines and comments will be ignored',
          'Go 2 blocks.',
          '# An interesting case - what about "Turn back", like on road?..',
          'Turn left.',
          'Go 1 block.'
]);
