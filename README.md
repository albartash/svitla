# About this project

This repository represents a solution for simple Robot Routing Interpreter System (RoRIS).

# Technical stack

* Python 3.6
* Peewee 3.10
* PostgreSQL 10

# How to use

## Setting up enviroment

To run the CLI, you need to provide environment variables:

* DB_NAME (required) - PostgreSQL database name
* DB_SCHEMA_NAME (optional) - PostgreSQL schema name. Defaults to 'public'

NOTE: The system believes you use *localhost* with port *5432*.

## Working with *.routes file

To load routes from *.routes file and see how it's interpreted, run:
```bash
python -m application.main /path/to/file.routes
```

## Working with the Database

To load routes from DB and see how it's interpreted, run:
```bash
python -m application.main db
```

In this case, the App will look into the table *routes* for existing routes.

## Obtaining results of running the CLI

By default, you can see the result of logging in <project_folder>/application/result.log.
You also can check result_from_*.log in the same folder as samples of such runs.

## Running tests

RoRIS uses standard *unittest* module for testing. That means, you can run tests by executing from your project folder:

```bash
python -m unittest
```

# Authors

* Alex Sokolov, Software Developer

# Copyright

Copyright &copy; 2019 Oleksandr Sokolov (@albartash). All rights reserved.
