import unittest

from application import instructions
from application import commands
from application.routing.state import RouteState
from application.routing.coordinates import Coordinates
from application.constants.directions import Direction
from application.constants.directions import TurnDirection


class CommandTestCase(unittest.TestCase):
    TURN_DIRECTIONS_MAP = {
        'right': TurnDirection.Right,
        'left': TurnDirection.Left
    }

    def setUp(self):
        self.route_state = RouteState

    def _check_start_command(self):
        start_coordinates: Coordinates = Coordinates(1, 3)
        instruction = instructions.StartRouteInstruction(start_coordinates)
        command = commands.StartCommand(state=self.route_state, instruction=instruction)
        command.execute()
        self.assertEqual(start_coordinates, self.route_state.current_position)
        self.assertEqual(self.route_state.current_direction, Direction.North)
        self.assertEqual(len(self.route_state.completed_actions), 0)

    def _check_turn_to_specific_direction(self, turn_direction: str, final_direction: Direction):
        instruction = instructions.TurnInstruction(turn_direction=turn_direction)
        command = commands.TurnCommand(state=self.route_state, instruction=instruction)
        command.execute()
        self.assertEqual(self.route_state.current_direction, final_direction)

    def _check_turn_command(self):
        for direction_args in (('right', Direction.East),
                               ('left', Direction.North)):
            self._check_turn_to_specific_direction(*direction_args)

    def _check_go_in_direction_command(self):
        instruction = instructions.GoInCurrentDirectionInstruction(distance=10)
        command = commands.GoInCurrentDirectionCommand(state=self.route_state, instruction=instruction)
        command.execute()
        self.assertEqual(self.route_state.current_position, Coordinates(1, 13))

    def _check_go_in_specified_direction_command(self):
        instruction = instructions.GoInSpecifiedDirectionInstruction(direction=Direction.South, distance=5)
        command = commands.GoInSpecifiedDirectionCommand(state=self.route_state, instruction=instruction)
        command.execute()
        self.assertEqual(self.route_state.current_position, Coordinates(1, 8))

    def test_commands(self):
        self._check_start_command()
        self._check_turn_command()
        self._check_go_in_direction_command()
        self._check_go_in_specified_direction_command()
