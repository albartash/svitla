import typing
import unittest

from application.constants.directions import Direction
from application.routing.coordinates import Coordinates
from application.math import calculate_new_point_after_movement


class CalculationTestCase(unittest.TestCase):

    def setUp(self):
        self.points = {
            Direction.North: {
                'initial': Coordinates(0, 0),
                'distance': 5,
                'result': Coordinates(0, 5)
            },
            Direction.South: {
                'initial': Coordinates(10, 20),
                'distance': 4,
                'result': Coordinates(10, 16)
            },
            Direction.East: {
                'initial': Coordinates(1, 7),
                'distance': 10,
                'result': Coordinates(11, 7)
            },
            Direction.West: {
                'initial': Coordinates(20, 40),
                'distance': 11,
                'result': Coordinates(9, 40)
            },
        }

    def _get_args_as_tuple(self, key: str) -> typing.Tuple[str, Coordinates, int]:
        return key, self.points[key]['initial'], self.points[key]['distance']

    def _get_result_by_direction(self, key: str) -> Coordinates:
        return self.points[key]['result']

    def _check_movement_to_direction(self, direction: str):
        self.assertEqual(calculate_new_point_after_movement(*self._get_args_as_tuple(direction)),
                         self._get_result_by_direction(direction))

    def test_movements_to_all_directions(self):
        for direction in self.points:
            self._check_movement_to_direction(direction)


class NegativeCoordinatesCalculationTestCase(unittest.TestCase):
    def setUp(self):
        self.points = {
            Direction.South: {
                'initial': Coordinates(0, 0),
                'distance': 4
            },
            Direction.West: {
                'initial': Coordinates(2, 4),
                'distance': 11
            },
        }

    def _get_args_as_tuple(self, key: Direction) -> typing.Tuple[Direction, Coordinates, int]:
        return key, self.points[key]['initial'], self.points[key]['distance']

    def test_negative_coordinates_movements(self):
        for direction in self.points:
            try:
                calculate_new_point_after_movement(*self._get_args_as_tuple(direction))
            except ValueError:
                pass
            else:
                self.fail(f"Negative coordinates movement is not working properly for direction {direction}")
