from enum import Enum

__all__ = ('Direction', 'TurnDirection', 'TURN_DIRECTIONS_MAP')


class Direction(Enum):
    North = "North"
    South = "South"
    West = "West"
    East = "East"


class TurnDirection(Enum):
    Right = "right"
    Left = "left"


TURN_DIRECTIONS_MAP = {
    Direction.North: {
        TurnDirection.Right.value: Direction.East,
        TurnDirection.Left.value: Direction.West
    },

    Direction.East: {
        TurnDirection.Right.value: Direction.South,
        TurnDirection.Left.value: Direction.North
    },

    Direction.South: {
        TurnDirection.Right.value: Direction.West,
        TurnDirection.Left.value: Direction.East
    },

    Direction.West: {
        TurnDirection.Right.value: Direction.North,
        TurnDirection.Left.value: Direction.South
    }
}
