import os


DB_NAME = os.environ['DB_NAME']
SCHEMA_NAME = os.environ.get('DB_SCHEMA_NAME', 'public')
