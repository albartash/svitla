from application.constants.directions import Direction
from application.routing.coordinates import Coordinates

__all__ = ('calculate_new_point_after_movement',)


DIRECTIONS_MOVEMENT_FUNCTIONS_MAP = {
    Direction.North: lambda coords, movement: Coordinates(coords.x, coords.y + movement),
    Direction.East: lambda coords, movement: Coordinates(coords.x + movement, coords.y),
    Direction.South: lambda coords, movement: Coordinates(coords.x, coords.y - movement),
    Direction.West: lambda coords, movement: Coordinates(coords.x - movement, coords.y)
}


def calculate_new_point_after_movement(direction: Direction, initial_coordinates: Coordinates, distance: int
                                       ) -> Coordinates:
    """Calculates new Coordinates object after moving in specified Direction from initial Coordinates


    Explanation of logic:

    +---+---+---+
    |   | X | Y |
    +---+---+---+
    | N | 0 | + |
    +---+---+---+
    | E | + | 0 |
    +---+---+---+
    | S | 0 | - |
    +---+---+---+
    | W | - | 0 |
    +---+---+---+

    "+" and "-" means "moving this coordinate"
    "0" means "do not use this coordinate"

    """

    coordinates = DIRECTIONS_MOVEMENT_FUNCTIONS_MAP[direction](initial_coordinates, distance)

    if coordinates.x < 0 or coordinates.y < 0:
        raise ValueError(f"Coordinates cannot be negative, got ({coordinates.x};{coordinates.y})")

    return coordinates
