import logging
import typing

from application.interfaces.routing import IRoute
from application.interfaces.instructions import IInstruction
from application.parser import validators
from application.exceptions import ValidationError
from application.routing.route import Route
from application.db import Route as RouteModel

__all__ = ('parse_routes_from_db', 'parse_routes_from_file')


logger = logging.getLogger(__name__)


def _parse_routes(lines) -> typing.List[IRoute]:
    routes = []

    route_initialized: bool = False
    route_started: bool = False
    current_route_instructions: typing.List[IInstruction] = []
    is_last_route_corrupted: bool = False

    for line in lines:
        try:
            validators.NonValuableLineValidator(line).validate()
            logger.debug(f'Skip comment line "{line}"')
            continue
        except ValidationError:
            pass

        try:
            validators.NewRouteLineValidator(line).validate()

            if route_initialized:  # Meaning - route is stopped here
                logger.debug("Creating new route")
                routes.append(Route(instructions=current_route_instructions))
                current_route_instructions = []

            # Starting a new route
            route_initialized = True
            route_started = False

            continue
        except ValidationError:
            pass

        if is_last_route_corrupted or not route_initialized:
            logger.debug(f'Skip line "{line}"')
            # We will skip route lines until the next route is initialized
            continue

        try:
            validator = validators.StartCommandValidator(line)
            validator.validate()
            route_started = True
            current_route_instructions.append(validator.validated_data)
            logger.debug(f'Started route. Line: "{line}"')
            continue
        except ValidationError:
            pass

        if not route_started:
            logger.debug(f'Route is not started. Skip line "{line}"')
            continue

        for validator_cls in (validators.GoInCurrentDirectionCommandLineValidator,
                              validators.GoInSpecifiedDirectionCommandLineValidator,
                              validators.GoToLandmarkCommandValidator,
                              validators.GoToLandmarkCommandValidator,
                              validators.TurnCommandValidator):
            try:
                validator = validator_cls(line)
                validator.validate()
                instruction = validator.validated_data
                is_last_route_corrupted = False
                logger.debug(f'Using validator {validator_cls} for line {line}')
                break
            except ValidationError:
                is_last_route_corrupted = True

        else:  # No validator has been passed
            logger.debug(f'Broken line: "{line}"')
            continue

        current_route_instructions.append(instruction)

    if route_initialized and route_started and current_route_instructions:
        routes.append(Route(instructions=current_route_instructions))
    return routes


def parse_routes_from_file(filename: str) -> typing.List[IRoute]:
    with open(filename, 'r') as f:
        # File object is also an iterator - let's use it for performance
        return _parse_routes(f)


def _get_routes_by_line(routes_from_db) -> typing.Iterable[str]:
    for route in routes_from_db:
        yield f"Route {route.name}:"
        for instruction in route.instructions:
            yield instruction


def parse_routes_from_db() -> typing.List[IRoute]:
    routes = RouteModel.select(RouteModel.name, RouteModel.instructions)
    return _parse_routes(_get_routes_by_line(routes))
