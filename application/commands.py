from abc import abstractmethod
import logging

from application.constants.directions import Direction
from application.constants.directions import TURN_DIRECTIONS_MAP
from application.interfaces.commands import ICommand
from application.interfaces.state import IRouteState
from application import instructions
from application.math import calculate_new_point_after_movement

__all__ = ('StartCommand', 'TurnCommand', 'GoInSpecifiedDirectionCommand', 'GoInCurrentDirectionCommand',
           'GoToLandmarkCommand')


logger = logging.getLogger(__name__)


class Command(ICommand):
    route_state: IRouteState

    def __init__(self, state: IRouteState):
        self.route_state = state

    @abstractmethod
    def execute(self):
        raise NotImplementedError


class StartCommand(Command):
    instruction: instructions.StartRouteInstruction

    def __init__(self, state: IRouteState, instruction: instructions.StartRouteInstruction):
        self.instruction = instruction
        super().__init__(state=state)

    def execute(self):
        self.route_state.current_position = self.instruction.start_coordinates
        self.route_state.current_direction = Direction.North  # A default one - this must be discussed
        self.route_state.completed_actions = []
        logger.info(f"START route at {self.route_state.current_position}")


class TurnCommand(Command):
    instruction: instructions.TurnInstruction

    def __init__(self, state: IRouteState, instruction: instructions.TurnInstruction):
        super().__init__(state=state)
        self.instruction = instruction

    def execute(self):
        logger.info(f"TURN from {self.route_state.current_direction} to {self.instruction.turn_to_direction}")
        self.route_state.current_direction = TURN_DIRECTIONS_MAP[self.route_state.current_direction][self.instruction.turn_to_direction]


class GoInCurrentDirectionCommand(Command):
    instruction: instructions.GoInCurrentDirectionInstruction

    def __init__(self, state: IRouteState, instruction: instructions.GoInCurrentDirectionInstruction):
        super().__init__(state=state)
        self.instruction = instruction

    def execute(self):
        logger.info(f"GO {self.instruction.distance} blocks to {self.route_state.current_direction}")
        self.route_state.current_position = calculate_new_point_after_movement(
            direction=self.route_state.current_direction,
            initial_coordinates=self.route_state.current_position,
            distance=self.instruction.distance
        )
        logger.debug(f"Now at {self.route_state.current_position}")


class GoInSpecifiedDirectionCommand(GoInCurrentDirectionCommand):
    instruction: instructions.GoInSpecifiedDirectionInstruction

    def __init__(self, state: IRouteState, instruction: instructions.GoInSpecifiedDirectionInstruction):
        super().__init__(state=state, instruction=instruction)

    def execute(self):
        logger.info(f"GO {self.instruction.distance} blocks to the {self.instruction.direction}")
        self.route_state.current_direction = self.instruction.direction
        self.route_state.current_position = calculate_new_point_after_movement(
            direction=self.route_state.current_direction,
            initial_coordinates=self.route_state.current_position,
            distance=self.instruction.distance
        )
        logger.debug(f"Now at {self.route_state.current_position}")


# Logic for Landmarks is not defined by business rules provided.
# The easiest solution here would be just hardly set Landmark's coordinates
# as a current position, but it's not good: looks a bit kinda mysterious teleportation...
class GoToLandmarkCommand(Command):
    instruction: instructions.GoToLandmarkInstruction

    def __init__(self, state: IRouteState, instruction: instructions.GoToLandmarkInstruction):
        super().__init__(state=state)
        self.instruction = instruction

    def execute(self):
        logger.warning("GO to Landmark IS NOT SUPPORTED NOW: THE BUSINESS RULES FOR MOVEMENTS TO THEM ARE NOT DEFINED")
        distance = 0
        logger.info(f'GO to {self.route_state.current_direction} until Landmark "{self.instruction.landmark_label}"')
        self.route_state.current_position = calculate_new_point_after_movement(
            direction=self.route_state.current_direction,
            initial_coordinates=self.route_state.current_position,
            distance=distance
        )
        logger.debug(f"Now at {self.route_state.current_position}")
        # raise NotImplementedError
