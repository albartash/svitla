import datetime

from application.config import DB_NAME
from application.config import SCHEMA_NAME
import peewee
from playhouse.postgres_ext import ArrayField

__all__ = ('Landmark', 'Route')


# https://github.com/coleifer/peewee/issues/292
class PointField(peewee.Field):
    db_field = 'point'


db = peewee.PostgresqlDatabase(database=DB_NAME)


class BaseModel(peewee.Model):
    created = peewee.TimestampField(default=datetime.datetime.utcnow)

    class Meta:
        database = db
        schema = SCHEMA_NAME


class Landmark(BaseModel):
    label = peewee.CharField(max_length=200)
    coordinates = PointField()

    class Meta:
        table_name = 'landmarks'


class Route(BaseModel):
    name = peewee.CharField(max_length=100)
    instructions = ArrayField(field_class=peewee.TextField)

    class Meta:
        table_name = 'routes'
