import typing

from application.interfaces.instructions import IInstruction
from application.interfaces.routing import IRoute

__all__ = ('Route',)


class Route(IRoute):
    _instructions: typing.List[IInstruction]

    def __init__(self, instructions: typing.List[IInstruction]):
        self._instructions = instructions

    @property
    def instructions(self) -> typing.List[IInstruction]:
        return self._instructions
