import typing

from application.interfaces.commands import ICommand
from application.interfaces.routing import IRoute
from application.interfaces.state import IRouteState

__all__ = ('RouteState',)


class RouteState(IRouteState):
    completed_actions: typing.Iterable[ICommand] = []

    def __init__(self, route: IRoute):
        self.route = route
