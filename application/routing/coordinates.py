from collections import namedtuple

__all__ = ('Coordinates',)

# Actually, we could use this: https://pypi.org/project/point2d/
# But didn't want to make extra dependency for third-party software
# for so non-important feature.

Coordinates = namedtuple('Coordinates', ('x', 'y'))
