from application.interfaces.routing import ILandmark
from application.routing.coordinates import Coordinates


class Landmark(ILandmark):
    label: str
    coordinates: Coordinates

    def __init__(self, label: str, coordinates: Coordinates):
        self.label = label
        self.coordinates = coordinates
