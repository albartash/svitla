from application.interfaces.routing import IRoute
from application.routing.state import RouteState
from application import commands
from application import instructions

__all__ = ('RouteInterpreter',)


class RouteInterpreter:
    route: IRoute

    def __init__(self, route: IRoute):
        self.route = route

    def interpret(self):
        """Interpretes the passed IRoute instance"""
        state = RouteState(route=self.route)

        # For this logic it was possible to implement __hash__()
        # for Instruction instances, but I think it will make it less flexible
        for instruction in self.route.instructions:
            if isinstance(instruction, instructions.StartRouteInstruction):
                command_cls = commands.StartCommand
            elif isinstance(instruction, instructions.TurnInstruction):
                command_cls = commands.TurnCommand
            elif isinstance(instruction, instructions.GoInCurrentDirectionInstruction):
                command_cls = commands.GoInCurrentDirectionCommand
            elif isinstance(instruction, instructions.GoInSpecifiedDirectionInstruction):
                command_cls = commands.GoInSpecifiedDirectionCommand
            elif isinstance(instruction, instructions.GoToLandmarkInstruction):
                command_cls = commands.GoToLandmarkCommand
            else:
                raise NotImplementedError(f"Unsupported instruction: {instruction}")

            command_cls(state=state, instruction=instruction).execute()
