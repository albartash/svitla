from application.constants.directions import Direction
from application.constants.directions import TurnDirection
from application.interfaces.instructions import IInstruction
from application.routing.coordinates import Coordinates

__all__ = ('StartRouteInstruction', 'TurnDirection', 'GoInSpecifiedDirectionInstruction',
           'GoInCurrentDirectionInstruction', 'GoToLandmarkInstruction')


class Instruction(IInstruction):
    pass


class StartRouteInstruction(Instruction):
    start_coordinates: Coordinates

    def __init__(self, coordinates: Coordinates):
        self.start_coordinates = coordinates


class TurnInstruction(Instruction):
    turn_to_direction: TurnDirection

    def __init__(self, turn_direction: TurnDirection):
        self.turn_to_direction = turn_direction


class GoInCurrentDirectionInstruction(Instruction):
    distance: int

    def __init__(self, distance: int):
        self.distance = distance


class GoInSpecifiedDirectionInstruction(GoInCurrentDirectionInstruction):
    direction: Direction

    def __init__(self, direction: Direction, distance: int):
        super().__init__(distance=distance)
        self.direction = direction


class GoToLandmarkInstruction(Instruction):
    landmark_label: str

    def __init__(self, label: str):
        self.landmark_label = label
