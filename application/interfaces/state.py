from abc import ABCMeta
import typing

from application.constants.directions import Direction
from application.interfaces.commands import ICommand
from application.interfaces.routing import IRoute
from application.routing.coordinates import Coordinates


class IRouteState(metaclass=ABCMeta):
    route: IRoute
    current_position: Coordinates
    current_direction: Direction
    completed_actions: typing.Iterable[ICommand]
