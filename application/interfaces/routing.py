from abc import ABCMeta
import typing

from application.interfaces.instructions import IInstruction


class IRoute(metaclass=ABCMeta):
    @property
    def instructions(self) -> typing.List[IInstruction]:
        raise NotImplementedError


class ILandmark(metaclass=ABCMeta):
    pass
