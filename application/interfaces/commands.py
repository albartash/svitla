from abc import ABCMeta
from abc import abstractmethod

__all__ = ('ICommand',)


class ICommand(metaclass=ABCMeta):
    @abstractmethod
    def execute(self):
        raise NotImplementedError
