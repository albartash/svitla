from abc import ABCMeta
from abc import abstractmethod

from application.interfaces.commands import ICommand


class ILineValidator(metaclass=ABCMeta):
    @abstractmethod
    def validate(self):
        """Validates a line from *.route file

        :raise: exceptions.ValidationError
        """
        raise NotImplementedError

    @property
    @abstractmethod
    def validated_data(self) -> ICommand:
        """Returns ICommand instance parsed from passed line"""
        raise NotImplementedError
