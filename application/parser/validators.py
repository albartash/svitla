from abc import ABCMeta
from abc import abstractmethod
import re

from application.exceptions import ValidationError
from application.interfaces.instructions import IInstruction
from application.interfaces.validators import ILineValidator
from application.routing.coordinates import Coordinates
from application import instructions


class BaseLineValidator(ILineValidator, metaclass=ABCMeta):
    regexp_pattern: str
    _validated_data: IInstruction = None
    _validated_regex_object = None

    def __init__(self, line: str):
        self._line = line

    @abstractmethod
    def _parse_command_from_line(self) -> IInstruction:
        """Parses ICommand instance from text line using regex pattern"""
        raise NotImplementedError

    def validate(self):
        self._validated_regex_object = re.fullmatch(self.regexp_pattern, self._line.strip(), flags=re.IGNORECASE)
        if not self._validated_regex_object:
            raise ValidationError(f"Invalid syntax in line: {self._line}")
        self._validated_data = self._parse_command_from_line()

    @property
    def validated_data(self) -> IInstruction:
        if not self._validated_regex_object:
            raise ValidationError("Property 'validated_data' must not be accessed before successful validation")
        return self._validated_data


class NonValuableLineValidator(BaseLineValidator):
    def validate(self):
        if not (self._line.startswith("#") or not self._line.strip()):
            raise ValidationError(f'Line "{self._line}" is valuable')

    def _parse_command_from_line(self) -> IInstruction:
        return None


class StartCommandValidator(BaseLineValidator):
    regexp_pattern: str = r"^Start\s+at\s+\(([0-9]{1,}),\s*([0-9]{1,})\)\s*.$"

    def _parse_command_from_line(self) -> IInstruction:
        coordinates_as_strings = self._validated_regex_object.groups()
        coords = Coordinates(int(coordinates_as_strings[0]), int(coordinates_as_strings[1]))
        return instructions.StartRouteInstruction(coordinates=coords)


class TurnCommandValidator(BaseLineValidator):
    regexp_pattern: str = r"^Turn\s+(right|left)\s*.$"

    def _parse_command_from_line(self) -> IInstruction:
        turn_direction = self._validated_regex_object.groups()[0]
        return instructions.TurnInstruction(turn_direction=turn_direction)


class GoInCurrentDirectionCommandLineValidator(BaseLineValidator):
    regexp_pattern: str = r"^Go\s+(1 block|[2-9]{1} blocks|[1-9]{2,} blocks)\s*.$"

    def _parse_command_from_line(self) -> IInstruction:
        distance: int = int(self._validated_regex_object.groups()[0].split()[0])
        return instructions.GoInCurrentDirectionInstruction(distance=distance)


class GoInSpecifiedDirectionCommandLineValidator(BaseLineValidator):
    regexp_pattern: str = r"^Go\s+(North|South|West|East)\s+(1 block|[2-9]{1} blocks|[1-9]{2,} blocks)\s*.$"

    def _parse_command_from_line(self) -> IInstruction:
        direction, distance = self._validated_regex_object.groups()
        distance = int(distance.split()[0])
        return instructions.GoInSpecifiedDirectionInstruction(direction=direction,
                                                              distance=distance)


class GoToLandmarkCommandValidator(BaseLineValidator):
    regexp_pattern: str = r'^Go\s+until\s+you\s+reach\s+landmark\s+\"([^"]+)\"\s*.$'

    def _parse_command_from_line(self) -> IInstruction:
        label = self._validated_regex_object.groups()[0]
        return instructions.GoToLandmarkInstruction(label=label)


class NewRouteLineValidator(BaseLineValidator):
    regexp_pattern: str = r"^Route .+:$"

    def _parse_command_from_line(self) -> IInstruction:
        return None
