import logging
import sys

from application.interpreter import RouteInterpreter
from application.utils import parse_routes_from_db
from application.utils import parse_routes_from_file


logging.basicConfig(level=logging.DEBUG, filename='result.log', filemode='w',
                    format='%(asctime)s - %(levelname)s - %(message)s')


logger = logging.getLogger(__name__)


def main():
    """This function is a simple entrypoint"""

    if len(sys.argv) < 2:
        logger.critical("You must specify *.route or *.routes file path, or use keyword 'db' for the database.")
        return 1
    elif len(sys.argv) > 2:
        logger.critical("Too much arguments. Stop.")
        return 2

    if sys.argv[1] == 'db':  # Use database
        routes = parse_routes_from_db()
    else:
        routes = parse_routes_from_file(sys.argv[1])

    for route in routes:
        logger.info("*** INTERPRETER START ***")
        interpreter = RouteInterpreter(route=route)
        interpreter.interpret()
        logger.info("*** INTERPRETER END ***")

    return 0


if __name__ == '__main__':
    sys.exit(main())
