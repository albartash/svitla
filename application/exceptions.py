__all__ = ('ValidationError',)


class ValidationError(Exception):
    def __init__(self, error_msg: str):
        self._error_msg = error_msg

    def __str__(self) -> str:
        return self._error_msg
